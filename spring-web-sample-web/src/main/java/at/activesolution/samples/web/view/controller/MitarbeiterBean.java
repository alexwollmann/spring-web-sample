package at.activesolution.samples.web.view.controller;

import at.activesolution.samples.web.core.model.domain.Mitarbeiter;
import at.activesolution.samples.web.core.service.api.IMitarbeiterService;
import org.springframework.context.annotation.Scope;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Bean zur Anzeige der Mitarbeiterliste und zum Bearbeiten von Mitarbeitern.
 */
@Named
@Scope("view")
public class MitarbeiterBean extends BaseBackingBean {

    @Inject
    private IMitarbeiterService mitarbeiterService;

    // aktuell bearbeiteter Mitarbeiter
    private Mitarbeiter currentMitarbeiter;

    // Liste aller Mitarbeiter
    private List<Mitarbeiter> mitarbeiterList;

    public List<Mitarbeiter> getMitarbeiterList() {
        if (mitarbeiterList == null) {
            mitarbeiterList = mitarbeiterService.listMitarbeiter();
        }
        return mitarbeiterList;
    }

    /**
     * Setzt einen Mitarbeiter zur Bearbeitung.
     *
     * @param mitarbeiter der zu bearbeitende Mitarbeiter
     */
    public void actionEdit(Mitarbeiter mitarbeiter) {
        this.currentMitarbeiter = mitarbeiter;
    }

    /**
     * Erstellt ein neues Mitarbeiter Objekt zur Anlage.
     */
    public void actionNew() {
        currentMitarbeiter = new Mitarbeiter();
    }

    /**
     * Loescht den uebergebenen Mitarbeiter aus der DB
     *
     * @param mitarbeiter zu loeschender Mitarbeiter
     */
    public void actionDelete(Mitarbeiter mitarbeiter) {
        try {
            mitarbeiterService.deleteMitarbeiter(mitarbeiter);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Mitarbeiter wurde gelöscht!"));

            reset();
        } catch (Exception e) {
            handleException(e);
        }
    }

    /**
     * Speichert den aktuell bearbeiteten Mitarbeiter.
     */
    public void actionSave() {
        try {
            mitarbeiterService.saveMitarbeiter(currentMitarbeiter);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Daten wurden gespeichert!"));

            reset();
        } catch (Exception e) {
            handleException(e);
        }
    }

    public void actionCancel() {
        reset();
    }

    /**
     * Mitarbeiterliste und aktuellen Mitarbeiter zuruecksetzen
     */
    private void reset() {
        currentMitarbeiter = null;
        mitarbeiterList = null;
    }

    public Mitarbeiter getCurrentMitarbeiter() {
        return currentMitarbeiter;
    }

    public void setCurrentMitarbeiter(Mitarbeiter currentMitarbeiter) {
        this.currentMitarbeiter = currentMitarbeiter;
    }

}
