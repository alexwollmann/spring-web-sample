package at.activesolution.samples.web.view.controller;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import at.activesolution.samples.web.core.model.criteria.MitarbeiterCriteria;
import at.activesolution.samples.web.core.model.domain.Mitarbeiter;
import at.activesolution.samples.web.core.service.api.IMitarbeiterService;

/**
 * Bean zur Suche nach Mitarbeitern.
 */
@Named
@Scope("view")
public class MitarbeiterSearchBean extends BaseBackingBean {

    @Inject
    private IMitarbeiterService mitarbeiterService;

    private MitarbeiterCriteria mitarbeiterCriteria = new MitarbeiterCriteria();

    // Liste aller Mitarbeiter
    private List<Mitarbeiter> mitarbeiterList;

    public List<Mitarbeiter> getMitarbeiterList() {
        return mitarbeiterList;
    }

    public MitarbeiterCriteria getMitarbeiterCriteria() {
        return mitarbeiterCriteria;
    }

    public void actionSearch() {
        mitarbeiterList = mitarbeiterService.searchMitarbeiter(mitarbeiterCriteria);
    }

    public void actionCancel() {
        mitarbeiterList = null;
        mitarbeiterCriteria = new MitarbeiterCriteria();
    }
}
