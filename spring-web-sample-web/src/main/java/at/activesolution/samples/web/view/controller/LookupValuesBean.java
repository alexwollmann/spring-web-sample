package at.activesolution.samples.web.view.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import at.activesolution.samples.web.core.service.api.IAbteilungService;

/**
 * Stellt die diversen SelectItems fuer Entities, etc. zur Verfuegung.
 */
@Named
@Scope("session")
public class LookupValuesBean {

    @Inject
    private IAbteilungService abteilungService;

    private List<SelectItem> abteilungen;

    // reset cached lists
    public void reset() {
        this.abteilungen = null;
    }

    /**
     * Liefert die Abteilungen als SelectItems.
     *
     * @return Liste von Abteilungen als SelectItems
     */
    public List<SelectItem> getAbteilungen() {
        if (abteilungen == null) {
            abteilungen = abteilungService.listAbteilungen().stream().map(a -> new SelectItem(a, a.getBezeichnung()))
                    .collect(Collectors.toList());
        }
        return abteilungen;
    }
}
