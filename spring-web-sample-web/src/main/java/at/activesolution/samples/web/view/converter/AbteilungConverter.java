package at.activesolution.samples.web.view.converter;

import at.activesolution.samples.web.core.model.domain.Abteilung;
import at.activesolution.samples.web.core.service.api.IAbteilungService;
import org.springframework.context.annotation.Scope;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Entity Converter fuer Abteilungen. Liefert die ID als Wert.
 */
@Named
@Scope("application")
public class AbteilungConverter implements Converter {

    @Inject
    private IAbteilungService abteilungService;

    /**
     * Aus JSF wird die ID als String uebergeben und hier ueber das Service in einen Entity umgewandelt.
     *
     * @param facesContext der FacesContext
     * @param uiComponent die Component
     * @param value eingegebener Wert
     * @return Abteilung-Entity oder <code>null</code>
     */
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }

        Long id = Long.valueOf(value);

        return abteilungService.getAbteilungById(id);
    }

    /**
     * Liefert die ID der Abteilung als String.
     *
     * @param facesContext der FacesContext
     * @param uiComponent die Component
     * @param object zu konvertierender Wert
     * @return ID der Abteilung
     */
    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object) {
        if (object == null) {
            return "";
        }

        return ((Abteilung) object).getId().toString();
    }
}
