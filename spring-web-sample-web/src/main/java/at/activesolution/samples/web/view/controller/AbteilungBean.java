package at.activesolution.samples.web.view.controller;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import at.activesolution.samples.web.core.model.domain.Abteilung;
import at.activesolution.samples.web.core.service.api.IAbteilungService;

/**
 * Bean zur Anzeige der Abteilungen und zum Bearbeiten dieser.
 */
@Named
@Scope("view")
public class AbteilungBean extends BaseBackingBean {

    @Inject
    private IAbteilungService abteilungService;

    // aktuell bearbeitete Abteilung
    private Abteilung currentAbteilung;

    // Liste aller Abteilungen
    private List<Abteilung> abteilungList;

    @Inject
    private LookupValuesBean lookupValuesBean;

    public List<Abteilung> getAbteilungList() {
        if (abteilungList == null) {
            abteilungList = abteilungService.listAbteilungen();
        }
        return abteilungList;
    }

    /**
     * Setzt einen Abteilung zur Bearbeitung.
     *
     * @param mitarbeiter der zu bearbeitende Abteilung
     */
    public void actionEdit(Abteilung mitarbeiter) {
        this.currentAbteilung = mitarbeiter;
    }

    /**
     * Erstellt ein neues Abteilung Objekt zur Anlage.
     */
    public void actionNew() {
        currentAbteilung = new Abteilung();
    }

    /**
     * Speichert den aktuell bearbeiteten Abteilung.
     */
    public void actionSave() {
        try {
            abteilungService.saveAbteilung(currentAbteilung);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Daten wurden gespeichert!"));

            reset();
            lookupValuesBean.reset();
        } catch (Exception e) {
            handleException(e);
        }
    }

    public void actionCancel() {
        reset();
    }

    /**
     * Abteilungliste und aktuellen Abteilung zuruecksetzen
     */
    private void reset() {
        currentAbteilung = null;
        abteilungList = null;
    }

    public Abteilung getCurrentAbteilung() {
        return currentAbteilung;
    }

    public void setCurrentAbteilung(Abteilung currentAbteilung) {
        this.currentAbteilung = currentAbteilung;
    }

}
