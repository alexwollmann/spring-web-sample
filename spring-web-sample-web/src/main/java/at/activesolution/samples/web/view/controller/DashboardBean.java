package at.activesolution.samples.web.view.controller;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.optionconfig.legend.Legend;
import org.primefaces.model.charts.pie.PieChartDataSet;
import org.primefaces.model.charts.pie.PieChartModel;
import org.primefaces.model.charts.pie.PieChartOptions;
import org.springframework.context.annotation.Scope;

import at.activesolution.samples.web.core.model.domain.Mitarbeiter;
import at.activesolution.samples.web.core.model.dto.AbteilungInfoDto;
import at.activesolution.samples.web.core.service.api.IAbteilungService;
import at.activesolution.samples.web.core.service.api.IMitarbeiterService;

/**
 * BackingBean fuer das Dashboard. Beinhaltet das Dashboard Model und die angezeigten Daten im Dashboard.
 */
@Named
@Scope("session")
public class DashboardBean extends BaseBackingBean {

    private DefaultDashboardModel model;

    @Inject
    private IMitarbeiterService mitarbeiterService;

    @Inject
    private IAbteilungService abteilungService;

    private List<Mitarbeiter> neueMitarbeiter;

    private PieChartModel abteilungChartModel;

    /**
     * Datenmodel fuer Dashboard (Rows/Columns)
     *
     * @return Model fuer Dashboard
     */
    public DashboardModel getModel() {
        if (model == null) {
            // DashboardModel initialisieren
            model = new DefaultDashboardModel();
            DashboardColumn column1 = new DefaultDashboardColumn();
            DashboardColumn column2 = new DefaultDashboardColumn();

            column1.addWidget("mitarbeiter");
            column1.addWidget("links");

            column2.addWidget("abteilungen");
            column2.addWidget("infos");

            model.addColumn(column1);
            model.addColumn(column2);
        }

        return model;
    }

    /**
     * Model fuer Abteilungs-Chart mit Mitarbeiter pro Abteilung
     *
     * @return Model fuer Chart
     */
    public PieChartModel getAbteilungChartModel() {
        if (abteilungChartModel == null) {
            abteilungChartModel = new PieChartModel();

            ChartData data = new ChartData();

            PieChartDataSet dataSet = new PieChartDataSet();
            List<Number> values = new ArrayList<>();
            List<String> labels = new ArrayList<>();

            List<String> bgColors = new ArrayList<>();
            bgColors.add("rgba(255, 99, 132, 0.8)");
            bgColors.add("rgba(255, 159, 64, 0.8)");
            bgColors.add("rgba(255, 205, 86, 0.8)");
            bgColors.add("rgba(75, 192, 192, 0.8)");
            bgColors.add("rgba(54, 162, 235, 0.8)");
            bgColors.add("rgba(153, 102, 255, 0.8)");
            bgColors.add("rgba(201, 203, 207, 0.8)");
            bgColors.add("rgba(255, 99, 132, 0.8)");
            bgColors.add("rgba(255, 159, 64, 0.8)");
            bgColors.add("rgba(255, 205, 86, 0.8)");
            bgColors.add("rgba(75, 192, 192, 0.8)");
            bgColors.add("rgba(54, 162, 235, 0.8)");
            bgColors.add("rgba(153, 102, 255, 0.8)");
            bgColors.add("rgba(201, 203, 207, 0.8)");
            dataSet.setBackgroundColor(bgColors);

            List<AbteilungInfoDto> abteilungInfo = abteilungService.getAbteilungInfo();
            for (AbteilungInfoDto dto : abteilungInfo) {
                values.add(dto.getAnzahlMitarbeiter());
                labels.add(dto.getAbteilung().getBezeichnung());
            }

            // Data
            dataSet.setData(values);
            data.addChartDataSet(dataSet);
            data.setLabels(labels);
            abteilungChartModel.setData(data);

            // Options
            Legend legend = new Legend();
            legend.setPosition("left");
            PieChartOptions options = new PieChartOptions();
            options.setLegend(legend);
            abteilungChartModel.setOptions(options);
        }
        return abteilungChartModel;
    }

    /**
     * Liefert die Liste der neuen Mitarbeiter
     *
     * @return Liste der neuen Mitarbeiter
     */
    public List<Mitarbeiter> getNeueMitarbeiter() {
        if (neueMitarbeiter == null) {
            neueMitarbeiter = mitarbeiterService.listNeueMitarbeiter();
        }
        return neueMitarbeiter;
    }

}
