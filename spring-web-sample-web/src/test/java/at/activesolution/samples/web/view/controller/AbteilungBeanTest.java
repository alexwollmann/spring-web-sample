package at.activesolution.samples.web.view.controller;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import at.activesolution.samples.web.core.model.domain.Abteilung;
import at.activesolution.samples.web.core.service.api.IAbteilungService;
import at.activesolution.samples.web.view.test.FacesContextTester;
import at.activesolution.samples.web.view.test.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AbteilungBeanTest {

    @InjectMocks
    private AbteilungBean abteilungBean;

    @Mock
    private IAbteilungService abteilungService;

    @Mock
    private LookupValuesBean lookupValuesBean;

    @Test
    @DisplayName("ActionNew erzeugt neue leere Abteilung")
    public void actionNewTest() {
        // init
        AbteilungBean abteilungBean = new AbteilungBean();
        Assertions.assertNull(abteilungBean.getCurrentAbteilung());

        // test
        abteilungBean.actionNew();

        // verify
        Assertions.assertNotNull(abteilungBean.getCurrentAbteilung());
        Assertions.assertNull(abteilungBean.getCurrentAbteilung().getBezeichnung());
        Assertions.assertNull(abteilungBean.getCurrentAbteilung().getId());
        Assertions.assertNull(abteilungBean.getCurrentAbteilung().getKuerzel());
    }

    @Test
    @DisplayName("ActionEdit uebernimmt Abteilung")
    public void actionEditTest() {
        // init
        AbteilungBean abteilungBean = new AbteilungBean();
        Assertions.assertNull(abteilungBean.getCurrentAbteilung());

        Abteilung myAbteilung = new Abteilung();
        myAbteilung.setId(23L);
        myAbteilung.setKuerzel("TEST");
        myAbteilung.setBezeichnung("Testabteilung");

        // test
        abteilungBean.actionEdit(myAbteilung);

        // verify
        Assertions.assertNotNull(abteilungBean.getCurrentAbteilung());
        Assertions.assertEquals(myAbteilung, abteilungBean.getCurrentAbteilung());
    }

    @Test
    @DisplayName("Save ruft das AbteilungService auf")
    public void actionSaveTest() {
        // init mock-facescontext
        FacesContext facesContext = Mockito.mock(FacesContext.class);
        FacesContextTester.setInstance(facesContext);

        // init testdate
        Abteilung myAbteilung = new Abteilung();
        myAbteilung.setId(23L);
        myAbteilung.setKuerzel("TEST");
        myAbteilung.setBezeichnung("Testabteilung");

        // Abteilung setezn
        abteilungBean.setCurrentAbteilung(myAbteilung);

        // save aurufen
        abteilungBean.actionSave();

        // save wurde mit dem richtigen objekt aufgerufen
        ArgumentCaptor<Abteilung> argumentCaptor = ArgumentCaptor.forClass(Abteilung.class);
        Mockito.verify(abteilungService).saveAbteilung(argumentCaptor.capture());
        Assertions.assertEquals(myAbteilung, argumentCaptor.getValue());

        // facesmessage wurde erstellt
        ArgumentCaptor<FacesMessage> facesMessageCaptor = ArgumentCaptor.forClass(FacesMessage.class);
        Mockito.verify(facesContext).addMessage(ArgumentMatchers.eq(null), facesMessageCaptor.capture());
        Assertions.assertEquals("Daten wurden gespeichert!", facesMessageCaptor.getValue().getSummary());

        // lookupValuesBean wurde zurueckgesetzt
        Mockito.verify(lookupValuesBean).reset();
    }

}
