package at.activesolution.samples.web.view.converter;

import at.activesolution.samples.web.core.model.domain.Abteilung;
import at.activesolution.samples.web.core.service.api.IAbteilungService;
import at.activesolution.samples.web.view.test.MockitoExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

/**
 * Tests fuer {@link AbteilungConverter}.
 */
@ExtendWith(MockitoExtension.class)
class AbteilungConverterTest {

    @InjectMocks
    private AbteilungConverter abteilungConverter;

    @Mock
    private IAbteilungService abteilungService;

    @Test
    @DisplayName("Converter Aufruf mit Null Value liefert Null")
    public void getAsObjectNullTest() {
        Object object = abteilungConverter.getAsObject(null, null, null);
        Assertions.assertNull(object, "Null Value liefert null");
    }

    @Test
    @DisplayName("Converter Aufruf mit Empty Value liefert Null")
    public void getAsObjectEmptyTest() {
        Object object = abteilungConverter.getAsObject(null, null, "");
        Assertions.assertNull(object, "Empty Value liefert null");
    }

    @Test
    @DisplayName("Converter Aufruf mit gueltiger ID liefert eine Abteilung")
    public void getAsObjectValidIdTest() {
        // Mock erstellen
        Abteilung abteilung = createAbteilung();
        Mockito.doReturn(abteilung).when(abteilungService).getAbteilungById(ArgumentMatchers.eq(23L));

        Object object = abteilungConverter.getAsObject(null, null, "23");
        Assertions.assertEquals(abteilung, object);
    }

    private Abteilung createAbteilung() {
        Abteilung abteilung = new Abteilung();
        abteilung.setId(23L);
        abteilung.setBezeichnung("TestAbt");
        return abteilung;
    }

    @Test
    @DisplayName("Converter Aufruf mit ungueltiger ID liefert null")
    public void getAsObjectInvalidIdTest() {
        // Mock erstellen
        Mockito.doReturn(null).when(abteilungService).getAbteilungById(ArgumentMatchers.eq(42L));

        Object object = abteilungConverter.getAsObject(null, null, "23");
        Assertions.assertNull(object);
    }

    @Test
    @DisplayName("Aufruf mit null-Object liefert Leerstring")
    public void getAsStringNullTest() {
        String string = abteilungConverter.getAsString(null, null, null);
        Assertions.assertEquals("", string);
    }

    @Test
    @DisplayName("Aufruf mit Abteilung-Object liefert ID der Abteilung")
    public void getAsStringAbteilungTest() {
        Abteilung abteilung = createAbteilung();

        String string = abteilungConverter.getAsString(null, null, abteilung);
        Assertions.assertEquals("23", string);
    }
}