create sequence if not exists hibernate_sequence start with 1000;

create table abteilung
(
	id IDENTITY NOT NULL CONSTRAINT abteilung_pk PRIMARY KEY,
	kuerzel varchar(10) not null,
	bezeichnung varchar(100) not null,
	status varchar(20) not null default 'AKTIV'
);

create table mitarbeiter
(
  id IDENTITY NOT NULL CONSTRAINT mitarbeiter_pk PRIMARY KEY,
	abteilung_id integer,
  vorname varchar(50) not null,
  nachname varchar(50) not null,
  geburtsdatum date,
  geschlecht varchar(20),
  eintrittsdatum date,
  detail varchar(1000),
  version integer not null default 0
);

alter table mitarbeiter
	add constraint mitarbeiter_abteilung_fk foreign key(abteilung_id) references abteilung(id);