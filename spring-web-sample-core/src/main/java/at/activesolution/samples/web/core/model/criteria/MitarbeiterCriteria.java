package at.activesolution.samples.web.core.model.criteria;

import java.io.Serializable;

import at.activesolution.samples.web.core.model.constants.Geschlecht;

/**
 * Suchkriterien fuer die Mitarbeiter-Suche.
 */
public class MitarbeiterCriteria implements Serializable {
    private String nachname;
    private String vorname;
    private Geschlecht geschlecht;
    private String abteilung;

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public Geschlecht getGeschlecht() {
        return geschlecht;
    }

    public void setGeschlecht(Geschlecht geschlecht) {
        this.geschlecht = geschlecht;
    }

    public String getAbteilung() {
        return abteilung;
    }

    public void setAbteilung(String abteilung) {
        this.abteilung = abteilung;
    }
}
