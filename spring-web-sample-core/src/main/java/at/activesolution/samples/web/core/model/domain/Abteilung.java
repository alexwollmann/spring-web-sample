package at.activesolution.samples.web.core.model.domain;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Entity Mapping fuer Tabelle "abteilung".
 */
@Entity
@Table(name = "abteilung")
public class Abteilung extends BaseEntity {

    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    @Size(max = 10)
    @Column(name = "kuerzel")
    private String kuerzel;

    @NotBlank
    @Size(max = 100)
    @Column(name = "bezeichnung")
    private String bezeichnung;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKuerzel() {
        return kuerzel;
    }

    public void setKuerzel(String kuerzel) {
        this.kuerzel = kuerzel;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
}
