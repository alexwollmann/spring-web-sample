package at.activesolution.samples.web.core.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import at.activesolution.samples.web.core.model.criteria.MitarbeiterCriteria;
import at.activesolution.samples.web.core.model.domain.Mitarbeiter;
import at.activesolution.samples.web.core.service.api.IMitarbeiterService;

/**
 * Implementierung des {@link IMitarbeiterService}.
 */
@Service
public class MitarbeiterService implements IMitarbeiterService {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Mitarbeiter> listMitarbeiter() {
        String select = "select m from Mitarbeiter m "
                + "left join fetch m.abteilung "
                + "order by m.nachname, m.vorname";
        TypedQuery<Mitarbeiter> query = entityManager.createQuery(select, Mitarbeiter.class);
        return query.getResultList();
    }

    @Override
    public List<Mitarbeiter> searchMitarbeiter(MitarbeiterCriteria mitarbeiterCriteria) {
        String where = "";
        List<String> queryClause = new ArrayList<>();
        Map<String, Object> queryParams = new HashMap<>();

        // Where Clause anhand der Kriterien erstellen
        if (mitarbeiterCriteria != null) {
            // LIKE-Clause fuer nachname
            if (mitarbeiterCriteria.getNachname() != null) {
                queryClause.add("m.nachname like :nachname");
                queryParams.put("nachname", "%" + mitarbeiterCriteria.getNachname() + "%");
            }
            // LIKE-Clause fuer vorname
            if (mitarbeiterCriteria.getVorname() != null) {
                queryClause.add("m.vorname like :vorname");
                queryParams.put("vorname", "%" + mitarbeiterCriteria.getVorname() + "%");
            }
            // EQUALS-Clause fuer Abteilung
            if (mitarbeiterCriteria.getAbteilung() != null) {
                queryClause.add("m.abteilung.kuerzel = :abteilung");
                queryParams.put("abteilung", mitarbeiterCriteria.getAbteilung());
            }
            // EQUALS-Clause fuer Geschlecht
            if (mitarbeiterCriteria.getGeschlecht() != null) {
                queryClause.add("m.geschlecht = :geschlecht");
                queryParams.put("geschlecht", mitarbeiterCriteria.getGeschlecht());
            }

            // aus der Query-Clause einen where-String erstellen. Es werden einfach die Elemente der Liste mit AND verknuepft
            if (!queryClause.isEmpty()) {
                where = " where " + String.join(" AND ", queryClause);
            }
        }

        String select = "select m from Mitarbeiter m "
                + "left join fetch m.abteilung "
                + where
                + " order by m.nachname, m.vorname";
        TypedQuery<Mitarbeiter> query = entityManager.createQuery(select, Mitarbeiter.class);

        // Parameter fuer WhereClause in Query uebernehmen
        queryParams.forEach((k, v) -> query.setParameter(k, v));

        return query.getResultList();
    }

    @Override
    public List<Mitarbeiter> listNeueMitarbeiter() {
        String select = "select m from Mitarbeiter m "
                + "join fetch m.abteilung "
                + "order by m.eintrittsdatum desc";
        TypedQuery<Mitarbeiter> query = entityManager.createQuery(select, Mitarbeiter.class);
        query.setMaxResults(10);
        return query.getResultList();
    }

    @Override
    @Transactional
    public Mitarbeiter saveMitarbeiter(Mitarbeiter mitarbeiter) {
        if (mitarbeiter.getId() == null) {
            entityManager.persist(mitarbeiter);
        } else {
            mitarbeiter = entityManager.merge(mitarbeiter);
        }

        return mitarbeiter;
    }

    @Override
    @Transactional
    public void deleteMitarbeiter(Mitarbeiter mitarbeiter) {
        Mitarbeiter mitarbeiterToDelete = mitarbeiter;
        if (!entityManager.contains(mitarbeiterToDelete)) {
            mitarbeiterToDelete = entityManager.find(Mitarbeiter.class, mitarbeiter.getId());
        }

        entityManager.remove(mitarbeiterToDelete);
    }
}
