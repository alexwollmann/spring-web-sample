package at.activesolution.samples.web.core.model.constants;

/**
 * Enum fuer Geschlecht (z.B. fuer Mitarbeiter)
 */
public enum Geschlecht {
    W, M;
}
