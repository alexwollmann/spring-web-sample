package at.activesolution.samples.web.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Spring Konfiguration fuer das Core-Paket<br>
 * Beinhaltet:
 * <ul>
 * <li>Component-Scan fuer die Service-Packages</li>
 * <li>Importiert die Perstistence Konfiguration {@link PersistenceConfig}</li>
 * </ul>
 */
@Configuration
@ComponentScan(basePackages = {"at.activesolution.samples.web.core.service"})
@Import(value = {PersistenceConfig.class})
public class CoreConfig {

}
