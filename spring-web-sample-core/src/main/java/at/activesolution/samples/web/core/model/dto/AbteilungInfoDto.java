package at.activesolution.samples.web.core.model.dto;

import at.activesolution.samples.web.core.model.domain.Abteilung;

import java.io.Serializable;

/**
 * DTO kapselt die Abteilung und die Anzahl deren Mitarbeiter.
 */
public class AbteilungInfoDto implements Serializable {
    private Abteilung abteilung;
    private Integer anzahlMitarbeiter;

    public AbteilungInfoDto() {
    }

    public AbteilungInfoDto(Abteilung abteilung, Long anzahlMitarbeiter) {
        this.abteilung = abteilung;
        this.anzahlMitarbeiter = anzahlMitarbeiter.intValue();
    }

    public Abteilung getAbteilung() {
        return abteilung;
    }

    public void setAbteilung(Abteilung abteilung) {
        this.abteilung = abteilung;
    }

    public Integer getAnzahlMitarbeiter() {
        return anzahlMitarbeiter;
    }

    public void setAnzahlMitarbeiter(Integer anzahlMitarbeiter) {
        this.anzahlMitarbeiter = anzahlMitarbeiter;
    }
}
