package at.activesolution.samples.web.core.service.api;

import at.activesolution.samples.web.core.model.domain.Abteilung;
import at.activesolution.samples.web.core.model.dto.AbteilungInfoDto;

import java.util.List;

/**
 * Service-Interface fuer den Zugriff auf Abteilungen.
 */
public interface IAbteilungService {

    /**
     * Liefert die Liste aller Abteilungen, sortiert nach der Bezeichnung.
     *
     * @return Liste von Abteilungen
     */
    List<Abteilung> listAbteilungen();

    /**
     * Sucht eine Abteilung anhand der ID
     *
     * @param id ID der Abteilung
     * @return gefundene Abteilung
     */
    Abteilung getAbteilungById(Long id);

    /**
     * Speichert eine Abteilung.
     *
     * @param abteilung Zu speichernde Abteilung
     * @return gespeicherte Abteilung
     */
    Abteilung saveAbteilung(Abteilung abteilung);

    /**
     * Liefert die Anzahl der Mitarbeiter je Abteilung
     * @return DTO mit AbteilungsInfos
     */
    List<AbteilungInfoDto> getAbteilungInfo();
}
