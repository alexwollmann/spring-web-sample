package at.activesolution.samples.web.core.service.api;

import java.util.List;

import at.activesolution.samples.web.core.model.criteria.MitarbeiterCriteria;
import at.activesolution.samples.web.core.model.domain.Mitarbeiter;

/**
 * Service-Interface fuer den Zugriff auf Mitarbeiter.
 */
public interface IMitarbeiterService {
    /**
     * Liefert die Liste aller Mitarbeiter, sortiert nach Nachname, Vorname.
     *
     * @return Liste aller Mitarbeiter
     */
    List<Mitarbeiter> listMitarbeiter();

    /**
     * Liefert die Liste der Mitarbeiter laut den uebergebenen Suchkriterien.
     *
     * @param mitarbeiterCriteria Suchkriterien fuer Mitarbeitersuche
     * @return Liste von Mitarbeitern
     */
    List<Mitarbeiter> searchMitarbeiter(MitarbeiterCriteria mitarbeiterCriteria);

    /**
     * Liefert die Liste der 10 neuesten Mitarbeiter laut Eintrittsdatum.
     *
     * @return Liste von Mitarbeitern
     */
    List<Mitarbeiter> listNeueMitarbeiter();

    /**
     * Speichert einen Mitarbeiter.
     *
     * @param mitarbeiter Zu speichernder Mitarbeiter
     * @return gespeicherter Mitarbeiter
     */
    Mitarbeiter saveMitarbeiter(Mitarbeiter mitarbeiter);

    /**
     * Loescht einen Mitarbeiter.
     *
     * @param mitarbeiter zu loeschender Mitarbeiter
     */
    void deleteMitarbeiter(Mitarbeiter mitarbeiter);
}
