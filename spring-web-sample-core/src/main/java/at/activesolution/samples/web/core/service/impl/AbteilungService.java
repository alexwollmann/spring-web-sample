package at.activesolution.samples.web.core.service.impl;

import at.activesolution.samples.web.core.model.domain.Abteilung;
import at.activesolution.samples.web.core.model.dto.AbteilungInfoDto;
import at.activesolution.samples.web.core.service.api.IAbteilungService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Implementierung des {@link IAbteilungService}.
 */
@Service
public class AbteilungService implements IAbteilungService {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Abteilung> listAbteilungen() {
        String select = "select a from Abteilung a order by a.bezeichnung";
        TypedQuery<Abteilung> query = entityManager.createQuery(select, Abteilung.class);
        return query.getResultList();
    }

    @Override
    public Abteilung getAbteilungById(Long id) {
        return entityManager.find(Abteilung.class, id);
    }

    @Override
    @Transactional
    public Abteilung saveAbteilung(Abteilung abteilung) {
        if (abteilung.getId() == null) {
            entityManager.persist(abteilung);
        } else {
            abteilung = entityManager.merge(abteilung);
        }

        return abteilung;
    }

    @Override
    public List<AbteilungInfoDto> getAbteilungInfo() {
        String select = "select new at.activesolution.samples.web.core.model.dto.AbteilungInfoDto(a, count(m)) " +
                "from Abteilung a, Mitarbeiter m where m.abteilung=a group by a order by a.bezeichnung";
        return entityManager.createQuery(select, AbteilungInfoDto.class).getResultList();
    }
}
