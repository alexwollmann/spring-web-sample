package at.activesolution.samples.web.core.model.domain;

import at.activesolution.samples.web.core.model.constants.Geschlecht;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Entity Mapping fuer Tabelle "mitarbeiter".
 */
@Entity
@Table(name = "mitarbeiter")
public class Mitarbeiter extends BaseEntity {

    @Id
    @GeneratedValue
    private Long id;

    @JoinColumn(name = "abteilung_id")
    @ManyToOne
    private Abteilung abteilung;

    @NotBlank
    @Size(max = 50)
    @Column(name = "vorname", length = 50, nullable = false)
    private String vorname;

    @NotBlank
    @Size(max = 50)
    @Column(name = "nachname", length = 50, nullable = false)
    private String nachname;

    @Temporal(TemporalType.DATE)
    @Column(name = "geburtsdatum")
    private Date geburtsdatum;

    @Enumerated(EnumType.STRING)
    @Column(name = "geschlecht")
    private Geschlecht geschlecht;

    @Temporal(TemporalType.DATE)
    @Column(name = "eintrittsdatum")
    private Date eintrittsdatum;

    @Size(max = 1000)
    @Column(name = "detail")
    private String detail;

    @Version
    private Integer version;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public Date getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(Date geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public Geschlecht getGeschlecht() {
        return geschlecht;
    }

    public void setGeschlecht(Geschlecht geschlecht) {
        this.geschlecht = geschlecht;
    }

    public Date getEintrittsdatum() {
        return eintrittsdatum;
    }

    public void setEintrittsdatum(Date eintrittsdatum) {
        this.eintrittsdatum = eintrittsdatum;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Abteilung getAbteilung() {
        return abteilung;
    }

    public void setAbteilung(Abteilung abteilung) {
        this.abteilung = abteilung;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
