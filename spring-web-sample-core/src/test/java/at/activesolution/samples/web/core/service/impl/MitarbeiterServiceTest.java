package at.activesolution.samples.web.core.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import at.activesolution.samples.web.core.config.CoreConfig;
import at.activesolution.samples.web.core.model.constants.Geschlecht;
import at.activesolution.samples.web.core.model.criteria.MitarbeiterCriteria;
import at.activesolution.samples.web.core.model.domain.Mitarbeiter;
import at.activesolution.samples.web.core.service.api.IMitarbeiterService;

/**
 * Test mit Spring-Support.<br>
 * Test laeuft in einer Transaktion, fuer die automatisch nach den einzelnen Tests
 * ein Rollback ausgeloest wird.
 * So beeinflussen sich die Daten der einzelnen Tests nicht gegenseitig.
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = CoreConfig.class)
@Transactional
public class MitarbeiterServiceTest {

    @Inject
    private IMitarbeiterService mitarbeiterService;

    @Test
    @DisplayName("Liste aller Mitarbeiter liefert 212 Eintraege in der richtigen Reihenfolge")
    public void listMitarbeiterTest() {
        // Liste der Mitarbeiter
        List<Mitarbeiter> mitarbeiterList = mitarbeiterService.listMitarbeiter();

        // Anzahl pruefen
        Assertions.assertEquals(212, mitarbeiterList.size());

        // ersten Mitarbeiter pruefen
        Mitarbeiter firstMitarbeiter = mitarbeiterList.get(0);
        Assertions.assertEquals("Albrecht", firstMitarbeiter.getNachname());
        Assertions.assertEquals("Bernd", firstMitarbeiter.getVorname());
        Assertions.assertEquals(Geschlecht.M, firstMitarbeiter.getGeschlecht());
    }

    @Test
    @DisplayName("Suche anhand des Namens liefert den richtigen Mitarbeiter")
    public void searchMitarbeiterNameTest() {
        // Criteria erstellen
        MitarbeiterCriteria mitarbeiterCriteria = new MitarbeiterCriteria();
        mitarbeiterCriteria.setNachname("anke"); // findet Franke
        mitarbeiterCriteria.setVorname("Isabel"); // findet Isabella

        // Service-Aufruf
        List<Mitarbeiter> mitarbeiterList = mitarbeiterService.searchMitarbeiter(mitarbeiterCriteria);

        // Anzahl pruefen
        Assertions.assertEquals(1, mitarbeiterList.size());

        // ersten Mitarbeiter pruefen
        Mitarbeiter firstMitarbeiter = mitarbeiterList.get(0);
        Assertions.assertEquals("Franke", firstMitarbeiter.getNachname());
        Assertions.assertEquals("Isabella", firstMitarbeiter.getVorname());
        Assertions.assertEquals(Geschlecht.W, firstMitarbeiter.getGeschlecht());
    }

    @Test
    @DisplayName("Suche anhand der Abteilung liefert alle Mitarbeiter dieser Abteilung")
    public void searchMitarbeiterAbteilungTest() {
        // Criteria erstellen
        MitarbeiterCriteria mitarbeiterCriteria = new MitarbeiterCriteria();
        mitarbeiterCriteria.setAbteilung("PUP");

        // Service-Aufruf
        List<Mitarbeiter> mitarbeiterList = mitarbeiterService.searchMitarbeiter(mitarbeiterCriteria);

        // Anzahl pruefen
        Assertions.assertEquals(20, mitarbeiterList.size());

        // ersten Mitarbeiter pruefen
        Mitarbeiter firstMitarbeiter = mitarbeiterList.get(0);
        Assertions.assertEquals("Albrecht", firstMitarbeiter.getNachname());
        Assertions.assertEquals("Manfred", firstMitarbeiter.getVorname());
        Assertions.assertEquals(Geschlecht.M, firstMitarbeiter.getGeschlecht());
    }

    @Test
    @DisplayName("Suche anhand des Geschlechts liefert alle Mitarbeiter mit diesem Geschlecht")
    public void searchMitarbeiterGeschlechtTest() {
        // Criteria erstellen
        MitarbeiterCriteria mitarbeiterCriteria = new MitarbeiterCriteria();
        mitarbeiterCriteria.setGeschlecht(Geschlecht.W);

        // Service-Aufruf
        List<Mitarbeiter> mitarbeiterList = mitarbeiterService.searchMitarbeiter(mitarbeiterCriteria);

        // Anzahl pruefen
        Assertions.assertEquals(102, mitarbeiterList.size());

        // ersten Mitarbeiter pruefen
        Mitarbeiter firstMitarbeiter = mitarbeiterList.get(0);
        Assertions.assertEquals("Albrecht", firstMitarbeiter.getNachname());
        Assertions.assertEquals("Viktoria", firstMitarbeiter.getVorname());
        Assertions.assertEquals(Geschlecht.W, firstMitarbeiter.getGeschlecht());
    }

    @Test
    @DisplayName("Suche mit leeren Kriterien liefert alle Mitarbeiter")
    public void searchMitarbeiterEmptyCriteriaTest() {
        // Criteria erstellen
        MitarbeiterCriteria mitarbeiterCriteria = new MitarbeiterCriteria();

        // Service-Aufruf
        List<Mitarbeiter> mitarbeiterList = mitarbeiterService.searchMitarbeiter(mitarbeiterCriteria);

        // Anzahl pruefen
        Assertions.assertEquals(212, mitarbeiterList.size());

        // ersten Mitarbeiter pruefen
        Mitarbeiter firstMitarbeiter = mitarbeiterList.get(0);
        Assertions.assertEquals("Albrecht", firstMitarbeiter.getNachname());
        Assertions.assertEquals("Bernd", firstMitarbeiter.getVorname());
        Assertions.assertEquals(Geschlecht.M, firstMitarbeiter.getGeschlecht());
    }

    @Test
    @DisplayName("Liefert die 10 neuesten Mitarbeiter")
    public void listNeueMitarbeiterTest() {
        // Liste der Mitarbeiter
        List<Mitarbeiter> mitarbeiterList = mitarbeiterService.listNeueMitarbeiter();

        // Anzahl pruefen
        Assertions.assertEquals(10, mitarbeiterList.size());

        // ersten Mitarbeiter pruefen
        Mitarbeiter firstMitarbeiter = mitarbeiterList.get(0);
        Assertions.assertEquals("Katrin", firstMitarbeiter.getVorname());
        Assertions.assertEquals("Frank", firstMitarbeiter.getNachname());
    }

    @Test
    @DisplayName("Speichern eines neuen Mitarbeiters testen")
    public void saveMitarbeiterTest() {
        Mitarbeiter mitarbeiter = new Mitarbeiter();
        mitarbeiter.setVorname("AaTest");
        mitarbeiter.setNachname("AaTest2");
        mitarbeiter.setGeschlecht(Geschlecht.W);

        Mitarbeiter savedMitarbeiter = mitarbeiterService.saveMitarbeiter(mitarbeiter);
        Assertions.assertNotNull(savedMitarbeiter);

        Mitarbeiter firstMa = mitarbeiterService.listMitarbeiter().get(0);

        Assertions.assertEquals(savedMitarbeiter, firstMa);
    }

    @Test
    @DisplayName("Loeschen eines Mitarbeiters testen")
    public void deleteMitarbeiterTest() {
        List<Mitarbeiter> mitarbeiterList = mitarbeiterService.listMitarbeiter();
        Mitarbeiter firstMa = mitarbeiterList.get(0);
        Mitarbeiter secondMa = mitarbeiterList.get(1);

        // loeschen
        mitarbeiterService.deleteMitarbeiter(firstMa);

        List<Mitarbeiter> mitarbeiterListAfterDelete = mitarbeiterService.listMitarbeiter();

        Assertions.assertEquals(secondMa, mitarbeiterListAfterDelete.get(0));
    }
}
