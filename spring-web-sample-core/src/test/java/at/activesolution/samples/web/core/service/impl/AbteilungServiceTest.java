package at.activesolution.samples.web.core.service.impl;

import at.activesolution.samples.web.core.config.CoreConfig;
import at.activesolution.samples.web.core.model.domain.Abteilung;
import at.activesolution.samples.web.core.model.dto.AbteilungInfoDto;
import at.activesolution.samples.web.core.service.api.IAbteilungService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

/**
 * Test mit Spring-Support.<br>
 * Test laeuft in einer Transaktion, fuer die automatisch nach den einzelnen Tests
 * ein Rollback ausgeloest wird.
 * So beeinflussen sich die Daten der einzelnen Tests nicht gegenseitig.
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = CoreConfig.class)
@Transactional
public class AbteilungServiceTest {

    @Inject
    private IAbteilungService abteilungService;

    @Test
    @DisplayName("Aufruf listAbteilungen liefert richtige Daten und Sortierung")
    public void listAbteilungenTest() {
        // Service liefert die Testdaten lt. test-data.sql
        List<Abteilung> abteilungen = abteilungService.listAbteilungen();

        // 8 Abteilungen gefunden
        Assertions.assertEquals(8, abteilungen.size());

        // pruefen, ob Kuerzel in der richtigen Reihenfolge
        Assertions.assertEquals("Anlagentechnik", abteilungen.get(0).getBezeichnung());
        Assertions.assertEquals("Elektrotechnik", abteilungen.get(1).getBezeichnung());
        Assertions.assertEquals("Faulenzen", abteilungen.get(2).getBezeichnung());
        Assertions.assertEquals("Maschinenbau", abteilungen.get(3).getBezeichnung());
        Assertions.assertEquals("Personal und Partner", abteilungen.get(4).getBezeichnung());
        Assertions.assertEquals("Softwareentwicklung .NET", abteilungen.get(5).getBezeichnung());
        Assertions.assertEquals("Softwareentwicklung Java", abteilungen.get(6).getBezeichnung());
        Assertions.assertEquals("Softwareentwicklung PHP", abteilungen.get(7).getBezeichnung());
    }

    @Test
    @DisplayName("Abteilung anhand der ID lesen")
    public void getAbteilungByIdTest() {
        Abteilung abteilung = abteilungService.getAbteilungById(1L);
        Assertions.assertNotNull(abteilung);
        Assertions.assertEquals("JAVA", abteilung.getKuerzel());
        Assertions.assertEquals("Softwareentwicklung Java", abteilung.getBezeichnung());
    }

    @Test
    public void getAbteilungInfoTest() {
        List<AbteilungInfoDto> abteilungInfos = abteilungService.getAbteilungInfo();

        Assertions.assertEquals(7, abteilungInfos.size());
        Assertions.assertEquals("AL", abteilungInfos.get(0).getAbteilung().getKuerzel());
        Assertions.assertEquals("ET", abteilungInfos.get(1).getAbteilung().getKuerzel());
        Assertions.assertEquals("MB", abteilungInfos.get(2).getAbteilung().getKuerzel());
        Assertions.assertEquals("PUP", abteilungInfos.get(3).getAbteilung().getKuerzel());
        Assertions.assertEquals(".NET", abteilungInfos.get(4).getAbteilung().getKuerzel());
        Assertions.assertEquals("JAVA", abteilungInfos.get(5).getAbteilung().getKuerzel());
        Assertions.assertEquals("PHP", abteilungInfos.get(6).getAbteilung().getKuerzel());
        Assertions.assertEquals(Integer.valueOf(18), abteilungInfos.get(0).getAnzahlMitarbeiter());
        Assertions.assertEquals(Integer.valueOf(17), abteilungInfos.get(1).getAnzahlMitarbeiter());
        Assertions.assertEquals(Integer.valueOf(12), abteilungInfos.get(2).getAnzahlMitarbeiter());
        Assertions.assertEquals(Integer.valueOf(20), abteilungInfos.get(3).getAnzahlMitarbeiter());
        Assertions.assertEquals(Integer.valueOf(17), abteilungInfos.get(4).getAnzahlMitarbeiter());
        Assertions.assertEquals(Integer.valueOf(16), abteilungInfos.get(5).getAnzahlMitarbeiter());
        Assertions.assertEquals(Integer.valueOf(18), abteilungInfos.get(6).getAnzahlMitarbeiter());
    }

    @Test
    public void saveTest() {
        Abteilung abteilung = new Abteilung();
        abteilung.setBezeichnung("Test Abteilung");
        abteilung.setKuerzel("TEST");

        Abteilung saved = abteilungService.saveAbteilung(abteilung);

        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(abteilung, saved);

        Long id = saved.getId();

        Abteilung loaded = abteilungService.getAbteilungById(id);

        Assertions.assertNotNull(loaded);
        Assertions.assertEquals("TEST", abteilung.getKuerzel());
        Assertions.assertEquals("Test Abteilung", abteilung.getBezeichnung());
    }


}